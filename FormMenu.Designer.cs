﻿namespace Client
{
    partial class FormMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TeamButton = new System.Windows.Forms.Button();
            this.MatchButton = new System.Windows.Forms.Button();
            this.TournamentButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TeamButton
            // 
            this.TeamButton.Location = new System.Drawing.Point(12, 12);
            this.TeamButton.Name = "TeamButton";
            this.TeamButton.Size = new System.Drawing.Size(75, 23);
            this.TeamButton.TabIndex = 0;
            this.TeamButton.Text = "Команда";
            this.TeamButton.UseVisualStyleBackColor = true;
            this.TeamButton.Click += new System.EventHandler(this.TeamButton_Click);
            // 
            // MatchButton
            // 
            this.MatchButton.Location = new System.Drawing.Point(12, 41);
            this.MatchButton.Name = "MatchButton";
            this.MatchButton.Size = new System.Drawing.Size(75, 23);
            this.MatchButton.TabIndex = 1;
            this.MatchButton.Text = "Матчи";
            this.MatchButton.UseVisualStyleBackColor = true;
            this.MatchButton.Click += new System.EventHandler(this.MatchButton_Click);
            // 
            // TournamentButton
            // 
            this.TournamentButton.Location = new System.Drawing.Point(12, 70);
            this.TournamentButton.Name = "TournamentButton";
            this.TournamentButton.Size = new System.Drawing.Size(75, 23);
            this.TournamentButton.TabIndex = 2;
            this.TournamentButton.Text = "Турниры";
            this.TournamentButton.UseVisualStyleBackColor = true;
            this.TournamentButton.Click += new System.EventHandler(this.TournamentButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(93, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Текущий рейтинг:";
            // 
            // FormMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 107);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TournamentButton);
            this.Controls.Add(this.MatchButton);
            this.Controls.Add(this.TeamButton);
            this.Name = "FormMenu";
            this.Text = "Меню";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button TeamButton;
        private System.Windows.Forms.Button MatchButton;
        private System.Windows.Forms.Button TournamentButton;
        private System.Windows.Forms.Label label1;
    }
}