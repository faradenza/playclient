﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class FormMenu : Form
    {
        private string playerName;
        public FormMenu(string playerName)
        {
            InitializeComponent();
            this.playerName = playerName;
        }

        private void TournamentButton_Click(object sender, EventArgs e)
        {
            FormTournament tournament = new FormTournament(playerName);
            tournament.Show();
        }

        private void TeamButton_Click(object sender, EventArgs e)
        {
            FormTeam team = new FormTeam(playerName);
            team.Show();
        }

        private void MatchButton_Click(object sender, EventArgs e)
        {
            FormMatch match = new FormMatch(playerName);
            match.Show();
        }
    }
}
