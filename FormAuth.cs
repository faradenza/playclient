﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class FormAuth : Form
    {
        ClientController clientController;
        string playerName;
        public FormAuth()
        {
            InitializeComponent();
            clientController = new ClientController();
        }


        private void RigisterButton_Click(object sender, EventArgs e)
        {
            string answer = clientController.RegisterPlayer(label1.Text, label2.Text);
            if(answer == "")
            {
                MessageBox.Show("Имя занято");
            }
            else
            {
                MessageBox.Show("Вы зарегистрированы");
            }
        }

        private void LogInButton_Click(object sender, EventArgs e)
        {
            string answer = clientController.LogIn(label1.Text, label2.Text);
            if (answer == "succes")
            {
                FormMenu menu = new FormMenu(playerName);
                menu.Show();
                Close();
            }
            else
            {
                MessageBox.Show("Не правильный пароль");
            }
            
        }
    }
}
