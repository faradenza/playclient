﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class FormTournament : Form
    {
        ClientController clientController;
        private string playerName;
        public FormTournament(string playerName)
        {
            InitializeComponent();
            clientController = new ClientController();
            this.playerName = playerName;
            listBoxTournament.Items.AddRange(clientController.GetTournamentList());
        }

        private void CreateTournamentButton_Click(object sender, EventArgs e)
        {
            clientController.CreateTournament(label1.Text, playerName);
        }

        private void ChooseTournamentButton_Click(object sender, EventArgs e)
        {

        }
    }
}
